from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_user, login_required, logout_user, current_user

from .models import User
from . import db

import re
from werkzeug.security import generate_password_hash, check_password_hash

auth = Blueprint("auth", import_name=__name__)


@auth.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        user = User.query.filter_by(email=email).first()

        if user:
            if check_password_hash(user.password, password):
                login_user(user, remember=True)
                flash("Logged in successfully", category="success")
                return redirect(url_for("views.home"))
            else:
                flash("Incorrect email or password. Please try again.", category="error")
        else:
            flash("Incorrect email or password. Please try again.", category="error")

    return render_template("login.html", user=current_user)

@auth.route("/logout")
@login_required
def logout():
    logout_user()

    return redirect(url_for("auth.login"))

@auth.route("/sign-up", methods=["GET", "POST"])
def sign_up():
    if request.method == "POST":
        email = request.form.get("email")
        firstName = request.form.get("firstName")
        password1 = request.form.get("password1")
        password2 = request.form.get("password2")

        user = User.query.filter_by(email=email).first()

        if user:
            flash("Email already exists", category="error")
        elif not re.search(".+@.+..+", email):
            flash('Email format invalid. Email adress must look like "john@gmail.com"', category='error')
        elif len(firstName) <= 2:
            flash('First name must be greater than 1 character.', category='error')
        elif password1 != password2:
            flash('Passwords don\'t match.', category='error')
        elif len(password1) <= 7:
            flash('Password must be at least 7 characters.', category='error')
        elif len(email) > 100 or len(firstName) > 100 or len(password1) > 100:
            flash("Email, first name and password must be shorter than 100 characters.", category="error")
        else:
            new_user = User(email=email, firstName=firstName, password=generate_password_hash(password1, method="sha256"))
            db.session.add(new_user)
            db.session.commit()

            login_user(user, remember=True)

            flash("Account successfuly created!", category="sucess")

            return redirect(url_for("views.home"))

    return render_template("sign-up.html", user=current_user)