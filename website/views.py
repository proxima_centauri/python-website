from flask import Blueprint, render_template, flash, request, jsonify
from flask_login import login_required, current_user

from sqlalchemy.sql.functions import user
import json

from .models import Note
from . import db


views = Blueprint("views", import_name=__name__)

@views.route("/", methods=["GET", "POST"])
@login_required
def home():
    if request.method == "POST":
        note = request.form.get("note")

        if len(note) < 3:
            flash("Note is too short!", category="error")
        elif len(note) > 10000:
            flash("Note is too long!", category="error")
        else:
            new_note = Note(data=note, user_id=current_user.id)
            db.session.add(new_note)
            db.session.commit()

            flash("Note successfully added")

    return render_template("home.html", user=current_user)

@views.route("/delete-note", methods=["POST"])
def delete_note():
    note = json.loads(request.data)
    noteId = note["noteId"]
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()
            flash("Note deleted", category="error")
    
    return jsonify({})